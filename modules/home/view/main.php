<!-- homepage hero
================================================== -->
<section id="hero">

 <div class="row hero-content">

   <div class="twelve columns hero-container">

      <!-- hero-slider start-->
      <div id="hero-slider" class="flexslider">

        <ul class="slides">

          <!-- slide -->
          <li>
            <div class="flex-caption">
             <h1 class="">Say hello to <span>Kreo</span>. We create awesome and stunning
               digital solutions.
             </h1>

             <h3 class="">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</h3>
           </div>
          </li>

          <!-- slide -->
          <li>
           <div class="flex-caption">
             <h1 class="">We are a small design studio dedicated to build great digital
               products.</h1>

             <h3 class="">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</h3>
           </div>
          </li>

          <!-- slide -->
          <li>
            <div class="flex-caption">
             <h1 class="">Take a look at some of <a class="smoothscroll" href="#portfolio" title="portfolio" >our works</a> or
             <a class="smoothscroll" href="#contact" title="contact us">get in touch</a> to discuss how we can help you.</h1>

             <h3 class="">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</h3>
           </div>
          </li>

        </ul>

      </div> <!-- end hero-slider -->

     </div> <!-- end twelve columns-->

 </div> <!-- end row -->

 <div id="more">
       <a class="smoothscroll" href="#services">More About Us<i class="fa fa-angle-down"></i></a>
 </div>

</section> <!-- end homepage hero -->
