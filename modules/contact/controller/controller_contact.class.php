
<?php
    class controller_contact {

        public function __construct() {
            $_SESSION['module'] = "contact";
            include(UTILS . "mail.inc.php");
        }

        public function view_contact() {
          require_once(VIEW_PATH_INC ."header.php");
    			require_once(VIEW_PATH_INC ."menu.php");

            loadView("modules/contact/view/", 'contact.php');

            require_once(VIEW_PATH_INC . "footer.html");
      			//require_once(VIEW_PATH_INC ."bottom_page.php");
        }
        public function send_mail(){
          $jsondata = array();
    	    $mail_content = json_decode($_POST["data_contact"], true);
          if($mail_content){
            //$json = send_mailgun($mail_content['email']);
             $json = send_mailgun($mail_content['email'],$mail_content['message'],$mail_content['subject'],$mail_content['name']);
            $jsondata['success'] = $json;
          }else{
            $jsondata['success'] = $json;
          }
          echo json_encode($jsondata);
        }
    }
