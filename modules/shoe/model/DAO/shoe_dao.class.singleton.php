<?php
class userDAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user_DAO($db, $arrArgument) {
        $name = $arrArgument['name'];
        $last_name = $arrArgument['last_name'];
        $address = $arrArgument['address'];
        $email = $arrArgument['email'];
        $brand= $arrArgument['brand'];
        $size = $arrArgument['size'];
        $color = $arrArgument['color'];
        $material = $arrArgument['material'];
        $price = $arrArgument['price'];
        $country = $arrArgument['country'];
        $province = $arrArgument['province'];
        $city = $arrArgument['city'];
        $avatar = $arrArgument['avatar'];

        $leather = 0;
        $cotton = 0;
        $patent_leather = 0;
        $rubber = 0;
        $other = 0;

        foreach ($material as $indice) {
            if ($indice === 'Leather')
                $leather = 1;
            if ($indice === 'Cotton')
                $cotton = 1;
            if ($indice === 'Patent_leather')
                $patent_leather = 1;
            if ($indice === 'Rubber')
                $rubber = 1;
            if ($indice === 'Other')
                $other = 1;
        }


        $sql = "INSERT INTO shoes (name, last_name, address, email,"
                . " brand, size, color, Leather,Cotton,Patent_leather,Rubber, Other,price,country, province,city, avatar"
                . " ) VALUES ('$name', '$last_name', '$address',"
                . " '$email', '$brand', '$size', '$color', '$leather', '$cotton', '$patent_leather', '$rubber', '$other', '$price', '$country', '$province','$city','$avatar')";

              //echo("$sql");
              //die();
        return $db->ejecutar($sql);
    }
    public function obtain_countries_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }

    public function obtain_provinces_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
          $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }

    public function obtain_cities_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
          $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }
}
