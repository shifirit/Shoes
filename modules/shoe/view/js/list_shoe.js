/*
function load_users_ajax() {
    $.ajax({
        type: 'GET',
        url: "modules/shoe/controller/controller_shoe.class.php?load=true",
        //dataType: 'json',
        async: false
    }).done(function (data) {
        var json = JSON.parse(data);
        //alert(json.user.usuario);

        pintar_user(json);

    }).fail(function (xhr) {
        alert(xhr.responseText);
    });
}
*/
function load_shoe() {
    //"index.php?module=users&function=load_users&load=true"
    var jqxhr = $.post("../../shoe/load_shoe/" , function (data) {
        var json = JSON.parse(data);
        //console.log(json);
        pintar_user(json);
        //alert( "success" );
    }).done(function () {
        //alert( "second success" );
    }).fail(function () {
        //alert( "error" );
    }).always(function () {
        //alert( "finished" );
    });
    jqxhr.always(function () {
        //alert( "second finished" );
    });
}

$(document).ready(function () {
    //load_users_ajax();
    load_shoe();
});

function pintar_user(data) {
    //alert(data.user.avatar);
    var content = document.getElementById("content");
    var div_shoe = document.createElement("div");
    var parrafo = document.createElement("p");

    var msje = document.createElement("div");
    msje.innerHTML = "msje = ";
    msje.innerHTML += data.msje;

    var name = document.createElement("div");
    name.innerHTML = "name = ";
    name.innerHTML += data.user.name;

    var last_name = document.createElement("div");
    last_name.innerHTML = "last_name = ";
    last_name.innerHTML += data.user.last_name;


    var address = document.createElement("div");
    address.innerHTML = "address = ";
    address.innerHTML += data.user.address;


    var email = document.createElement("div");
    email.innerHTML = "email = ";
    email.innerHTML += data.user.email;

    var brand = document.createElement("div");
    brand.innerHTML = "brand = ";
    brand.innerHTML += data.user.brand;


    var size = document.createElement("div");
    size.innerHTML = "size = ";
    size.innerHTML += data.user.size;

    var color = document.createElement("div");
    color.innerHTML = "color = ";
    color.innerHTML += data.user.color;

    var material = document.createElement("div");
    material.innerHTML = "material = ";
    for(var i =0;i < data.user.material.length;i++){
    material.innerHTML += " - "+data.user.material[i];
    }
    var price = document.createElement("div");
    price.innerHTML = "price = ";
    price.innerHTML += data.user.price;

    var country = document.createElement("div");
    country.innerHTML = "country = ";
    country.innerHTML += data.user.country;

    var province = document.createElement("div");
    province.innerHTML = "province = ";
    province.innerHTML += data.user.province;

    var city = document.createElement("div");
    city.innerHTML = "city = ";
    city.innerHTML += data.user.city;


    //arreglar ruta IMATGE!!!!!

    var cad = data.user.avatar;
    //console.log(cad);
    //var cad = cad.toLowerCase();
    var img = document.createElement("div");
    var html = '<img src="' + cad + '" height="75" width="75"> ';
    img.innerHTML = html;
    //alert(html);

    div_shoe.appendChild(parrafo);
    parrafo.appendChild(msje);
    parrafo.appendChild(name);
    parrafo.appendChild(last_name);
    parrafo.appendChild(address);
    parrafo.appendChild(email);
    parrafo.appendChild(brand);
    parrafo.appendChild(size);
    parrafo.appendChild(color);
    parrafo.appendChild(material);
    parrafo.appendChild(price);
    parrafo.appendChild(country);
    parrafo.appendChild(province);
    parrafo.appendChild(city);
    content.appendChild(div_shoe);
    content.appendChild(img);
}
