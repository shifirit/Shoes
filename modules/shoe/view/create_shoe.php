<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">
<!-- Script with absolute route -->

<script type="text/javascript" src="<?php echo SHOE_JS_PATH ?>shoe.js" ></script>
<section id="contact-page">
    <div class="container">
        <div class="center">
            <h2>ADD SHOE    </h2>
            <p class="lead">Welcome to the sale of second hand shoes online.</p>
        </div>
        <div class="row contact-wrap">
            <div class="status alert alert-success" style="display: none"></div>
            <form id="form" method="post">
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" id="name" name="name" placeholder="name" class="form-control" required="required" value="name">

                    </div>
                    <div class="form-group">
                        <label>Last Name *</label>
                        <input type="text" id="last_name" name="last_name" placeholder="last name" class="form-control" required="required" value="last_name">

                    </div>


                    <div class="form-group">
                        <label>Address*</label><br />
                        <input id="address" type="text" name="address" placeholder="Street nXX ptaX" required="required" class="form-control" value="adress"  >
                    </div>
                </div>
                <div class="col-sm-5">

                    <div class="form-group">
                        <label>E-mail *</label>
                        <input type="email" id="email" name="email" placeholder="e-mail" class="form-control" required="required" value="email">

                    </div>
                    <div class="form-group">
                        <label>Brand*</label><br>

                        Addidas  <input type="radio" id="brand" name="brand" value="addidas">
                        Nikke <input type="radio" id="brand" name="brand" value="nikke">
                        Vans  <input type="radio" id="brand"  name="brand" value="vans">
                        Convers  <input type="radio" id="brand"  name="brand" value="convers">
                        Reboot  <input type="radio" id="brand"  name="brand" value="reboot">
                        Other   <input type="radio" id="brand"  name="brand" value="Other" checked="checked">


                    </div>
                    <div class="form-group">
                        <label>Shoe Size</label><br />
                        <select name="size" id="size">
                            <option selected>Select size</option>
                            <option value="kids">For kids</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="46">47</option>
                            <option value="47">+47</option>
                        </select>

                    </div>
                    <div>
                      <label>Select a color:</label><br>
                       <input name="color" id= "color" type="color" value="#000000"/>

                    </div>
                    <div>
                    <div class="form-group">
                        <label>Material  *</label><br>

                        Leather  <input type="checkbox" name="material[]" class="material" value="Leather">
                        Cotton  <input type="checkbox" name="material[]" class="material" value="Cotton">
                        Patent leather  <input type="checkbox" name="material[]" class="material" value="Patent_leather">
                        Rubber   <input type="checkbox" name="material[]" class="material" value="Rubber">
                        Other   <input type="checkbox" name="material[]" class="material" value="Other" checked="checked">

                    </div>
                    <div>
                      <label> Price </label>
                    <input type="text" id="price" name="price" size="4" placeholder="Price shoe" maxlength="3" style="text-align: left">




                  </div>
                  <div>
                  <label>Origin:</label>
                    <tr>
                        <td>Country: </td>
            			  <td id="error_country">
            			    <select name="country" id="country">
            			    <option selected>Select country</option>
            				</select>
            				<div ></div>
            			</td>
                    </tr>
                    <tr>
                      <td> </td>
                    </tr>
                    <tr>
                        <td>Province: </td>
            			  <td id="error_province">
            			    <select name="province" id="province">
            			    <option selected>Select province</option>
            				</select>
            				<div></div>
            			</td>
                    </tr>
                    <tr>
                      <td> </td>
                    </tr>
                    <tr>
                        <td>City: </td>
            			  <td id="error_city">
            			    <select name="city" id="city">
            			    <option selected>Select city</option>
            				</select>
            				<div></div>
            			</td>
                  </div>

                  <br />
                  <br />
                  <br />
                  <br />
                  <div class="form-group" id="progress">
                      <div id="bar"></div>
                      <div id="percent">0%</div >
                  </div>

                  <div class="msg"></div>
                  <br/>
                  <div id="dropzone" class="dropzone"></div><br/>
                  <br/>
                  <br/>
                  <br/>

                    <div class="form-group">
                        <button type="button" id="submit_shoe" name="submit_shoe" class="btn btn-primary btn-lg" value="submit">Submit Message</button>
                    </div>

                </div>
            </form>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#contact-page-->
