<?php

class controller_shoe {


  function __construct() {
     //include(FUNCTIONS_SHOE . "functions_shoe.inc.php");
    //  include(UTILS . "upload.php");
      $_SESSION['module'] = "shoe";
  }

 function  form_shoe() {

      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView('modules/shoe/view/' , 'create_shoe.php');

      require_once(VIEW_PATH_INC . "footer.html");
  }
  function results_shoe() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");


      loadView('modules/shoe/view/', 'results_shoe.php');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  //////////////////////////////////////////////////////////////// upload
  function upload_shoe() {

     include(UTILS . "upload.php");
    if ((isset($_GET["upload"])) && ($_GET["upload"] == true)) {
      $result_avatar = upload_files();
      $_SESSION['result_avatar'] = $result_avatar;
      //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de dropzone.js
    }
  }

  function alta_shoe() {

      include(FUNCTIONS_SHOE . "functions_shoe.inc.php");
      $jsondata = array();
      $shoeJSON = json_decode($_POST["alta_shoe_json"], true);
      $result = validate_shoe($shoeJSON);
    //  $result=true;

      if (empty($_SESSION['result_avatar'])) {
          $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => 'media/default-avatar.png');
      }
      $result_avatar = $_SESSION['result_avatar'];

      if (($result['resultado']) && ($result_avatar['resultado'])) {
      //  echo json_encode($result);
      //  exit;

        $arrArgument = array(
            'name' => ucfirst($result['datos']['name']),
            'last_name' => ucfirst($result['datos']['last_name']),
            'address' => $result['datos']['address'],
            'email' => $result['datos']['email'],
            'brand' => $result['datos']['brand'],
            'size' => strtoupper($result['datos']['size']),
            'color' => $result ['datos']['color'],
            'material' => $result['datos']['material'],
            'price' => $result['datos']['price'],
            'country' => $result['datos']['country'],
            'province' => $result['datos']['province'],
            'city' => $result['datos']['city'],
            'avatar' => $result_avatar['datos']
        );

            /////////////////insert into BD////////////////////////
            $arrValue = false;
            try{
            //$path_model = $_SERVER['DOCUMENT_ROOT'] . '/proyects/Kreo10/modules/shoe/model/model/';
            $arrValue = loadModel(MODEL_SHOE, "shoe_model", "create_shoe", $arrArgument);
            //echo json_encode($arrValue);
            //die();
          } catch (Exception $e) {
            showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);

          }

        if ($arrValue)
            $mensaje = "Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
        else
              $mensaje = "No se ha podido realizar su alta. Intentelo mas tarde";


              $_SESSION['user'] = $arrArgument;
              $_SESSION['msje'] = $mensaje;
              //$callback = "index.php?module=shoe&view=results_shoe";
              $callback= "../../shoe/results_shoe/";

              $jsondata["success"] = true;
              $jsondata["redirect"] = $callback;
              echo json_encode($jsondata);
          exit;
      } else {
          //$error = $result['error'];
          //$error_avatar = $result_avatar['error'];
          $jsondata["success"] = false;
          $jsondata["error"] = $result['error'];
          $jsondata["error_avatar"] = $result_avatar['error'];

          $jsondata["success1"] = false;
          if ($result_avatar['resultado']) {
              $jsondata["success1"] = true;
              $jsondata["img_avatar"] = $result_avatar['datos'];
          }
          header('HTTP/1.0 400 Bad error');
          echo json_encode($jsondata);
          //exit;
      }
  }
  //////////////////////////////////////////////////////////////// delete
function delete_shoe(){
   include(UTILS . "upload.php");
  if (isset($_POST["delete"]) && $_POST["delete"] == true) {
      $_SESSION['result_avatar'] = array();
      $result = remove_files();
      if ($result === true) {
          echo json_encode(array("res" => true));
      } else {
          echo json_encode(array("res" => false));
      }
  }
}


  //////////////////////////////////////////////////////////////// load

function load_shoe(){
//  if (isset($_GET["load"]) && $_GET["load"] == true) {
      $jsondata = array();
      if (isset($_SESSION['user'])) {
          //echo debug($_SESSION['user']);
          $jsondata["user"] = $_SESSION['user'];
      }
      if (isset($_SESSION['msje'])) {
          //echo $_SESSION['msje'];
          $jsondata["msje"] = $_SESSION['msje'];
      }
      close_session();
      echo json_encode($jsondata);
      exit;
  //}
}

  function close_session() {
      unset($_SESSION['user']);
      unset($_SESSION['msje']);
      $_SESSION = array(); // Destruye todas las variables de la sesión
      session_destroy(); // Destruye la sesión
  }


  /////////////////////////////////////////////////// load_data
function load_data_shoe(){
  //if ((isset($_GET["load_data"])) && ($_GET["load_data"] == true)) {
      $jsondata = array();
      if (isset($_SESSION['user'])) {
          $jsondata["user"] = $_SESSION['user'];
          echo json_encode($jsondata);
          exit;
      } else {
          $jsondata["user"] = "";
          echo json_encode($jsondata);
          exit;
      }
  //}
}
  /////////////////////////////////////////////////// load_country
function load_countries_shoe(){
  //if(  (isset($_GET["load_country"])) && ($_GET["load_country"] == true)  ){
      $json = array();
      $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

      //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/proyects/Kreo10/modules/shoe/model/model/';

      try {
          //throw new Exception();
          $json = loadModel(MODEL_SHOE, "shoe_model", "obtain_countries", $url);
      } catch (Exception $e) {
          $json = array();
      }

      if($json){
        echo $json;
        exit;
      }else{
        $json = "error";
        echo $json;
        exit;
      }
  //  }
}

  /////////////////////////////////////////////////// load_provinces

function load_provinces_shoe(){
  if(  (isset($_GET["load_provinces"])) && ($_GET["load_provinces"] == true)  ){
        $jsondata = array();
          $json = array();

      //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/proyects/Kreo10/modules/shoe/model/model/';
    try {
      $json = loadModel(MODEL_SHOE, "shoe_model", "obtain_provinces");
    } catch (Exception $e) {
      $json = array();
    }

      if($json){
        $jsondata["provinces"] = $json;
        echo json_encode($jsondata);
        exit;
      }else{
        $jsondata["provinces"] = "error";
        echo json_encode($jsondata);
        exit;
      }
    }
}
  /////////////////////////////////////////////////// load_cities
function load_towns_shoe(){
  if(isset($_POST['idPoblac']) ){
        $jsondata = array();
        $json = array();

      //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/proyects/Kreo10/modules/shoe/model/model/';

      try{
      $json = loadModel(MODEL_SHOE, "shoe_model", "obtain_cities", $_POST['idPoblac']);

    } catch (Exception $e){
      showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
      if($json){
        $jsondata["cities"] = $json;
        echo json_encode($jsondata);
        exit;
      }else{
        $jsondata["cities"] = "error";
        echo json_encode($jsondata);
        exit;
      }
    }
  }

}
/*


    //////////////////////////////////////////////////////////////// alta_users_json
    if ((isset($_POST['alta_shoe_json']))) {
        alta_shoe();
    }



}
