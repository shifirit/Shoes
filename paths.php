<?php
//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/proyects/Kreo10/';
define('SITE_ROOT', $path);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/proyects/Kreo10/');

//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/js/');

//IMG
define('IMG_PATH', SITE_PATH . 'view/images/');

//model
define('MODEL_PATH', SITE_ROOT . 'model/');

//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');

define('PRODUCTION', true);




//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');
//resources
define('RESOURCES', SITE_ROOT . 'resources/');
//media
define('MEDIA_PATH', SITE_ROOT . 'media/');
//utils
define('UTILS', SITE_ROOT . 'utils/');

//model shoe backend
define('FUNCTIONS_SHOE', SITE_ROOT . 'modules/shoe/utils/');
define('MODEL_PATH_SHOE', SITE_ROOT . 'modules/shoe/model/');
define('DAO_SHOE', SITE_ROOT . 'modules/shoe/model/DAO/');
define('BLL_SHOE', SITE_ROOT . 'modules/shoe/model/BLL/');
define('MODEL_SHOE', SITE_ROOT . 'modules/shoe/model/model/');
define('SHOE_JS_PATH', SITE_PATH . 'modules/shoe/view/js/');


//model shoe frontend
define('UTILS_SHOE1', SITE_ROOT . 'modules/shoe1/utils/');
//define('SHOE1_JS_LIB_PATH', SITE_PATH . 'modules/products/view/lib/');
define('SHOE1_JS_PATH', SITE_PATH . 'modules/shoe1/view/js/');
define('MODEL_PATH_SHOE1', SITE_ROOT . 'modules/shoe1/model/');
define('DAO_SHOE1', SITE_ROOT . 'modules/shoe1/model/DAO/');
define('BLL_SHOE1', SITE_ROOT . 'modules/shoe1/model/BLL/');
define('MODEL_SHOE1', SITE_ROOT . 'modules/shoe1/model/model/');

//MODEL CONTACT
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');

//amigables
define('URL_AMIGABLES', TRUE);
