alert("Hola");
jQuery.fn.fill_or_clean = function () {
    this.each(function () {
        if ($("#name").val() == "") {
            $("#name").val("Introduce name");
            $("#name").focus(function () {
                if ($("#name").val() == "Introduce name") {
                    $("#name").val("");
                }
            });
        }
        $("#name").blur(function () { //Onblur se activa cuando el usuario retira el foco
            if ($("#name").val() == "") {
                $("#name").val("Introduce name");
            }
        });

        if ($("#last_name").val() == "") {
            $("#last_name").val("Introduce last name");
            $("#last_name").focus(function () {
                if ($("#last_name").val() == "Introduce last name") {
                    $("#last_name").val("");
                }
            });
        }
        $("#last_name").blur(function () {
            if ($("#last_name").val() == "") {
                $("#last_name").val("Introduce last name");
            }
        });

        if ($("#address").val() == "") {
            $("#address").val("Introduce address");
            $("#address").focus(function () {
                if ($("#address").val() == "Introduce address") {
                    $("#address").val("");
                }
            });
        }
        $("#address").blur(function () {
            if ($("#address").val() == "") {
                $("#address").val("Introduce address");
            }
        });

        if ($("#email").val() == "") {
            $("#email").val("Introduce email");
            $("#email").focus(function () {
                if ($("#email").val() == "Introduce email") {
                    $("#email").val("");
                }
            });
        }
        $("#email").blur(function () {
            if ($("#email").val() == "") {
                $("#email").val("Introduce email");
            }
        });

        if ($("#price").val() == "") {
            $("#price").val("Introduce a correct price");
            $("#price").focus(function () {
                if ($("#price").val() == "Introduce a correct price") {
                    $("#price").val("");
                }
            });
        }
        $("#price").blur(function () {
            if ($("#price").val() == "") {
                $("#price").val("Introduce a correct price");
            }
        });

    });//each
    return this;
};//function

Dropzone.autoDiscover = false;
$(document).ready(function () {

  //Valida users /////////////////////////
  $('#submit_shoe').click(function () {
      validate_user();
  });

  //Control de seguridad para evitar que al volver atrás de la pantalla results a create, no nos imprima los datos
  $.get("../../shoe/load_data_shoe/",
          function (response) {
              //alert(response.user);
              if (response.user === "") {
                  $("#name").val('');
                  $("#last_name").val('');
                  $("#address").val('');
                  $("#email").val('');
                  $("#brand").val('');
                  $("#size").val('Shoe Size');
                  $("#color").val('');
                  var inputElements = document.getElementsByClassName('messageCheckbox');
                  for (var i = 0; i < inputElements.length; i++) {
                      if (inputElements[i].checked) {
                          inputElements[i].checked = false;
                      }
                  }
                  $("#price").val('');
                  $('#country').val('Select country');
                  $('#province').val('Select province');
                  $('#city').val('Select city');
                  //siempre que creemos un plugin debemos llamarlo, sino no funcionará
                  $(this).fill_or_clean();
                              } else {
                                  $("#name").val( response.user.name);
                                  $("#last_name").val( response.user.last_name);
                                  $("#address").val( response.user.address);
                                  $("#email").val( response.user.email);
                                  $("#brand").val(response.user.brand);

                                  $("#size").val( response.user.size);
                                  $("#color").val( response.user.color);
                                  var material = response.user.material;
                                  var inputElements = document.getElementsByClassName('material');
                                  for (var i = 0; i < material.length; i++) {
                                      for (var j = 0; j < inputElements.length; j++) {
                                          if(material[i] ===inputElements[j] )
                                              inputElements[j].checked = true;
                                      }
                                  }
                                  $("#price").val( response.user.price);
                                  $('#country').val(response.user.country);
                                  $('#province').val(response.user.province);
                                  $('#city').val(response.user.city);
                              }
                          }, "json");

  //Dropzone function //////////////////////////////////
                  $("#dropzone").dropzone({
                      //url: "modules/shoe/controller/controller_shoe.class.php?upload=true",
                      url: "../../shoe/upload_shoe/",
                      params:{'upload':true},
                      addRemoveLinks: true,
                      maxFileSize: 1000,
                      dictResponseError: "Ha ocurrido un error en el server",
                      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
                      init: function () {
                          this.on("success", function (file, response) {
                              //alert(response);
                              $("#progress").show();
                              $("#bar").width('100%');
                              $("#percent").html('100%');
                              $('.msg').text('').removeClass('msg_error');
                              $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                          });
                      },
                      complete: function (file) {
                          //if(file.status == "success"){
                          //alert("El archivo se ha subido correctamente: " + file.name);
                          //}
                      },
                      error: function (file) {
                          //alert("Error subiendo el archivo " + file.name);
                      },
                      removedfile: function (file, serverFileName) {
                          var name = file.name;
                          $.ajax({
                              type: "POST",
                              //url: "modules/shoe/controller/controller_shoe.class.php?delete=true",
                              //data: "filename=" + name,
                              url: "../../shoe/delete_shoe/",
                              data: {"filename":name,"delete":true},
                              success: function (data) {
                                  $("#progress").hide();
                                  $('.msg').text('').removeClass('msg_ok');
                                  $('.msg').text('').removeClass('msg_error');
                                  $("#e_avatar").html("");

                                  var json = JSON.parse(data);
                                  if (json.res === true) {
                                      var element;
                                      if ((element = file.previewElement) != null) {
                                          element.parentNode.removeChild(file.previewElement);
                                          //alert("Imagen eliminada: " + name);
                                      } else {
                                          false;
                                      }
                                  } else { //json.res == false, elimino la imagen también
                                      var element;
                                      if ((element = file.previewElement) != null) {
                                          element.parentNode.removeChild(file.previewElement);
                                      } else {
                                          false;
                                      }
                                  }
                              }
                          });
                      }
                  });


    //$(this).fill_or_clean(); //siempre que creemos un plugin debemos llamarlo, sino no funcionará

  //Utilizamos las expresiones regulares para las funciones de  fadeout
    var email_reg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var address_reg = /^[a-z0-9- -.]+$/i;
    var string_reg = /^[A-Za-z]{2,30}$/;
    var price_reg = /^[0-9]+$/;

    //realizamos funciones para que sea más práctico nuestro formulario
    $("#name, #last_name").keyup(function () {
        if ($(this).val() != "" && string_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#address").keyup(function () {
        if ($(this).val() != "" && address_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#email").keyup(function () {
        if ($(this).val() != "" && email_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#price").keyup(function () {
        if ($(this).val() != "" && price_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

  //Dependent combos //////////////////////////////////
  load_countries_v1();

  $("#province").empty();
  $("#province").append('<option value="" selected="selected">Select province</option>');
  $("#province").prop('disabled', true);
  $("#city").empty();
  $("#city").append('<option value="" selected="selected">Select city</option>');
  $("#city").prop('disabled', true);

  $("#country").change(function() {
  var country = $(this).val();
  var province = $("#province");
  var city = $("#city");

  if(country !== 'ES'){
         province.prop('disabled', true);
         city.prop('disabled', true);
         $("#province").empty();
       $("#city").empty();
  }else{
         province.prop('disabled', false);
         city.prop('disabled', false);
         load_provinces_v2();
  }//fi else
});

$("#province").change(function() {
  var prov = $(this).val();
  if(prov > 0){
    load_cities_v2(prov);
  }else{
    $("#city").prop('disabled', false);
  }
});

});//End document ready

  function validate_user() {
      var result = true;

      var name = document.getElementById('name').value;
      //alert(name);
      var last_name = document.getElementById('last_name').value;
      //alert(last_name);
      var address = document.getElementById('address').value;
      //alert(address);
      var email = document.getElementById('email').value;
      //alert(email);
      var brand = document.getElementById('brand').value;
      //var brand = $('input[name="brad"]:checked').val();
      alert(brand);


      var size = document.getElementById('size').value;
      //alert(size);
      var color = document.getElementById('color').value;
      //alert(color);
      var material = [];
      var inputElements = document.getElementsByClassName('material');
      var j = 0;
      for (var i = 0; i < inputElements.length; i++) {
          if (inputElements[i].checked) {
              material[j] = inputElements[i].value;
              j++;
          }
      }
      //alert(material);
      var price = document.getElementById('price').value;
      //alert(price);
      var country = document.getElementById('country').value;
      var province = document.getElementById('province').value;
      var city = document.getElementById('city').value;
      //Utilizamos las expresiones regulares para la validación de errores JS
      var email_reg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
      var address_reg = /^[a-z0-9- -.]+$/i
      var string_reg = /^[A-Za-z]{2,30}$/;
      var price_reg = /^[0-9]+$/;

      $(".error").remove();

      if ($("#name").val() == "" || $("#name").val() == "Introduce name") {
          $("#name").focus().after("<span class='error'>Introduce name</span>");
          result = false;
          return false;
      } else if (!string_reg.test($("#name").val())) {
          $("#name").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
          result = false;
          return false;
      }

      else if ($("#last_name").val() == "" || $("#last_name").val() == "Introduce last name") {
          $("#last_name").focus().after("<span class='error'>Introduce last name</span>");
          result = false;
          return false;
      } else if (!string_reg.test($("#last_name").val())) {
          $("#last_name").focus().after("<span class='error'>Last name must be 2 to 30 letters</span>");
          result = false;
          return false;
      }


      if ($("#address").val() == "" || $("#address").val() == "Introduce address") {
          $("#address").focus().after("<span class='error'>Introduce address</span>");
          result = false;
          return false;
      } else if (!address_reg.test($("#address").val())) {
          $("#address").focus().after("<span class='error'>Address don't have  symbols.</span>");
          result = false;
          return false;
      }


      if ($("#email").val() == "" || $("#email").val() == "Introduce email") {
          $("#email").focus().after("<span class='error'>Introduce email</span>");
          result = false;
          return false;
      } else if (!email_reg.test($("#email").val())) {
          $("#email").focus().after("<span class='error'>Error format email (example@example.com).</span>");
          result = false;
          return false;
      }
      if ($("#size").val() === "" || $("#size").val() === "Select size" || $("#size").val() === null) {
          $("#size").focus().after("<span class='error'>Select one size</span>");
          return false;
      }
      if ($("#color").val() === "" || $("#color").val() === "#000000" || $("#color").val() === null) {
          $("#color").focus().after("<span class='error'>Select one color</span>");
          return false;
      }

      if ($("#price").val() == "" || $("#price").val() == "Introduce a price") {
          $("#price").focus().after("<span class='error'>Introduce a price</span>");
          return false;
      } else if (!price_reg.test($("#price").val())) {
          $("#price").focus().after("<span class='error'>Error isnt a correct price .</span>");
          return false;
      }
      if ($("#country").val() === "" || $("#country").val() === "Select country" || $("#country").val() === null) {
          $("#country").focus().after("<span class='error'>Select one country</span>");
          return false;
      }

      if ($("#province").val() === "" || $("#province").val() === "Select province") {
          $("#province").focus().after("<span class='error'>Select one province</span>");
          return false;
      }

      if ($("#city").val() === "" || $("#city").val() === "Select city") {
          $("#city").focus().after("<span class='error'>Select one city</span>");
          return false;
      }

      //Si ha ido todo bien, se envian los datos al servidor
      if (result) {
        if (province === null) {
            province = 'default_province';
        }else if (province.length === 0) {
            province = 'default_province';
        }else if (province === 'Select province') {
            return 'default_province';
        }

        if (city === null) {
            city = 'default_city';
        }else if (city.length === 0) {
            city = 'default_city';
        }else if (city === 'Select city') {
            return 'default_city';
        }

          var data = {"name": name, "last_name": last_name, "address": address,  "email": email, "brand": brand,
          "size": size, "color": color, "material": material, "price": price, "country": country, "province": province, "city": city};

          var data_users_JSON = JSON.stringify(data);

          $.post("../../shoe/alta_shoe/",
                  {alta_shoe_json: data_users_JSON},
          function (response) {
            console.log(response);

              if (response.success) {
                  window.location.href = response.redirect;
              }
              //alert(response);  //para debuguear
              //}); //para debuguear
          //}, "json").fail(function (xhr) {

          }, "json").fail(function(xhr, status, error) {
              console.log(xhr.responseText);
              console.log(xhr.responseJSON);

              if (xhr.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (xhr.status == 404) {
                    alert('Requested page not found [404]');
                } else if (xhr.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + xhr.responseText);
                }
              if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null )
                      xhr.responseJSON = JSON.parse(xhr.responseText);


              if (xhr.responseJSON.error.name)
                  $("#name").focus().after("<span  class='error1'>" + xhr.responseJSON.error.name + "</span>");

              if (xhr.responseJSON.error.last_name)
                  $("#last_name").focus().after("<span  class='error1'>" + xhr.responseJSON.error.last_name + "</span>");

              if (xhr.responseJSON.error.address)
                  $("#address").focus().after("<span  class='error1'>" + xhr.responseJSON.error.address + "</span>");

              if (xhr.responseJSON.error.email)
                  $("#email").focus().after("<span  class='error1'>" + xhr.responseJSON.error.email + "</span>");

              if (xhr.responseJSON.error.brand)
                  $("#brand").focus().after("<span class='error1'>" + xhr.responseJSON.error.brand + "</span>");

              if (xhr.responseJSON.error.size)
                  $("#size").focus().after("<span  class='error1'>" + xhr.responseJSON.error.size + "</span>");

              if (xhr.responseJSON.error.color)
                  $("#color").focus().after("<span  class='error1'>" + xhr.responseJSON.error.color + "</span>");

              if (xhr.responseJSON.error.material)
                  $("#e_material").focus().after("<span  class='error1'>" + xhr.responseJSON.error.material + "</span>");

              if (xhr.responseJSON.error.price)
                  $("#price").focus().after("<span  class='error1'>" + xhr.responseJSON.error.price + "</span>");

              if(xhr.responseJSON.error.country)
                  $("#error_country").focus().after("<span  class='error1'>" + xhr.responseJSON.error.country + "</span>");

              if(xhr.responseJSON.error.province)
                  $("#error_province").focus().after("<span  class='error1'>" + xhr.responseJSON.error.province + "</span>");

              if(xhr.responseJSON.error.city)
                  $("#error_city").focus().after("<span  class='error1'>" + xhr.responseJSON.error.city + "</span>");

              if (xhr.responseJSON.error_avatar)
                  $("#dropzone").focus().after("<span  class='error1'>" + xhr.responseJSON.error_avatar + "</span>");

              if (xhr.responseJSON.success1) {
                  if (xhr.responseJSON.img_avatar !== "/media/default-avatar.png") {
                      //$("#progress").show();
                      //$("#bar").width('100%');
                      //$("#percent").html('100%');
                      //$('.msg').text('').removeClass('msg_error');
                      //$('.msg').text('Success Upload image!!').addClass('msg_ok').animate({ 'right' : '300px' }, 300);
                  }
              } else {
                  $("#progress").hide();
                  $('.msg').text('').removeClass('msg_ok');
                  $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
              }
          });
      }
  }

  function load_countries_v2(cad) {
      $.getJSON( cad, function(data) {
        $("#country").empty();
        $("#country").append('<option value="" selected="selected">Select country</option>');

        $.each(data, function (i, valor) {
          $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
        });
      })
      .fail(function() {
          alert( "error load_countries" );
      });
  }

  function load_countries_v1() {
      $.get( "../../shoe/load_countries_shoe/",{'load_country':true},
          function( response ) {
              //console.log(response);
              if(response === 'error'){
                  load_countries_v2("../../resources/ListOfCountryNamesByName.json");
              }else{
                  load_countries_v2("../../shoe/load_countries_shoe/",{'load_country':true}); //oorsprong.org
                  //load_countries_v2("resources/ListOfCountryNamesByName.json");
              }
      })
      .fail(function(response) {
          load_countries_v2("../../resources/ListOfCountryNamesByName.json");
      });
  }

  function load_provinces_v2() {
      $.get("../../resources/provinciasypoblaciones.xml", function (xml) {
  	    $("#province").empty();
  	    $("#province").append('<option value="" selected="selected">Select province</option>');

          $(xml).find("provincia").each(function () {
              var id = $(this).attr('id');
              var name = $(this).find('nombre').text();
              $("#province").append("<option value='" + id + "'>" + name + "</option>");
          });
      })
      .fail(function() {
          alert( "error load_provinces" );
      });
  }

  function load_provinces_v1() { //provinciasypoblaciones.xml - xpath
      $.get('../../shoe/load_provinces_shoe/',{'load_provinces':true},
          function( response ) {
            $("#province").empty();
  	        $("#province").append('<option value="" selected="selected">Select province</option>');

              //alert(response);
          var json = JSON.parse(response);
  		    var provinces=json.provinces;
  		    //alert(provinces);
  		    //console.log(provinces);

  		    //alert(provinces[0].id);
  		    //alert(provinces[0].nombre);

              if(provinces === 'error'){
                  load_provinces_v2();
              }else{
                  for (var i = 0; i < provinces.length; i++) {
          		    $("#province").append("<option value='" + provinces[i].id + "'>" + provinces[i].nombre + "</option>");
      		    }
              }
      })
      .fail(function(response) {
          load_provinces_v2();
      });
  }

  function load_cities_v2(prov) {
      $.get("../../resources/provinciasypoblaciones.xml", function (xml) {
  		$("#city").empty();
  	    $("#city").append('<option value="" selected="selected">Select city</option>');

  		$(xml).find('provincia[id=' + prov + ']').each(function(){
      		$(this).find('localidad').each(function(){
      			 $("#city").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
      		});
          });
  	})
  	.fail(function() {
          alert( "error load_cities" );
      });
  }

  function load_cities_v1(prov) { //provinciasypoblaciones.xml - xpath
      var datos = { idPoblac : prov  };
  	$.post("../../shoe/load_towns_shoe/", datos, function(response) {
  	    //alert(response);
          var json = JSON.parse(response);
  		var cities=json.cities;
  		//alert(poblaciones);
  		//console.log(poblaciones);
  		//alert(poblaciones[0].poblacion);

  		$("#city").empty();
  	    $("#city").append('<option value="" selected="selected">Select city</option>');

          if(cities === 'error'){
              load_cities_v2(prov);
          }else{
              for (var i = 0; i < cities.length; i++) {
          		$("#city").append("<option value='" + cities[i].poblacion + "'>" + cities[i].poblacion + "</option>");
      		}
          }
  	})
  	.fail(function() {
          load_cities_v2(prov);
      });
  }











/*

    $("#submit_shoe").click(function () {
        $(".error").remove();
        if ($("#name").val() == "" || $("#name").val() == "Introduce name") {
            $("#name").focus().after("<span class='error'>Introduce name</span>");
            return false;
        } else if (!string_reg.test($("#name").val())) {
            $("#name").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
            return false;
        }

        else if ($("#last_name").val() == "" || $("#last_name").val() == "Introduce last name") {
            $("#last_name").focus().after("<span class='error'>Introduce last name</span>");
            return false;
        } else if (!string_reg.test($("#last_name").val())) {
            $("#last_name").focus().after("<span class='error'>Last name must be 2 to 30 letters</span>");
            return false;
        }


        if ($("#address").val() == "" || $("#address").val() == "Introduce address") {
            $("#address").focus().after("<span class='error'>Introduce address</span>");
            return false;
        } else if (!address_reg.test($("#address").val())) {
            $("#address").focus().after("<span class='error'>Address don't have  symbols.</span>");
            return false;
        }


        if ($("#email").val() == "" || $("#email").val() == "Introduce email") {
            $("#email").focus().after("<span class='error'>Introduce email</span>");
            return false;
        } else if (!email_reg.test($("#email").val())) {
            $("#email").focus().after("<span class='error'>Error format email (example@example.com).</span>");
            return false;
        }

        if ($("#price").val() == "" || $("#price").val() == "Introduce a price") {
            $("#price").focus().after("<span class='error'>Introduce a price</span>");
            return false;
        } else if (!price_reg.test($("#price").val())) {
            $("#price").focus().after("<span class='error'>Error isnt a correct price .</span>");
            return false;
        }

        $("#form").submit();
        $("#form").attr("action", "index.php?module=shoe");

    });
    */
