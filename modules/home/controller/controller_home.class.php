<?php
class controller_home {

    function __construct() {
        //include(UTILS . "common.inc.php");
    }

    function begin() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/home/view/', 'main.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

}
