<?php
class shoe_model {
    private $bll;
    static $_instance;

    private function __construct() {
       include(BLL_SHOE1 . "shoe_bll.class.singleton.php");
        $this->bll = shoe_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }


    public function list_shoe($limit) {
        return $this->bll->list_shoe_BLL($limit);
    }

    public function list_one_shoe($id) {
        return $this->bll->list_one_shoe_BLL($id);
    }



}
