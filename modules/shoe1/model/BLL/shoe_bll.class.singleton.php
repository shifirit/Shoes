<?php
class shoe_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
       include(DAO_SHOE1 . "shoe_dao.class.singleton.php");
       include(MODEL_PATH . "Db.class.singleton.php");
        $this->dao = shoeDAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_shoe_BLL($limit){
      return $this->dao->list_shoe_DAO($this->db,$limit);
    }

    public function list_one_shoe_BLL($id){
      return $this->dao->list_one_shoe_DAO($this->db,$id);
    }

    public function details_shoe_BLL($id){
      return $this->dao->details_shoe_DAO($this->db,$id);
    }
}
