<?php

function validate_shoe($value) {
    $error = array();
    $valido = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'last_name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'address' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-z0-9- ]+$/i')
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'valida_email'
        ),
        'price' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]+$/')
        ),
    );


    $resultado = filter_var_array($value, $filtro);

    //no filter
    $resultado['brand'] = $value['brand'];
    $resultado['size'] = $value['size'];
    $resultado['color'] = $value['color'];
    $resultado['material'] = $value['material'];
    $resultado['country'] = $value['country'];
    $resultado['province'] = $value['province'];
    $resultado['city'] = $value['city'];




    if ($resultado['size'] === 'Select size') {
        $error['size'] = "You haven't select size.";
        $valido = false;
    }

    if ($resultado['color'] === '#000000') {
        $error['color'] = "You haven't select a color.";
        $valido = false;
    }

    if(count($resultado['material']) < 1){
        $error['material'] = "Select 1 or more.";
        $valido = false;
    }

    if(count($resultado['brand']) < 1 ){
        $error['brand'] = " * No has seleccionado ninguna Marca";
        $valido = false;
    }

    if ($resultado['country']==='Select country'){
            $error['country']="You need to choose a country";
            $valido = false;
        }

    if ($resultado['province']==='Select province'){
            $error['province']="You need to choose a province";
            $valido = false;
        }

    if ($resultado['city']==='Select city'){
            $error['city']="You need to choose a city";
            $valido = false;
        }

    if ($resultado != null && $resultado) {


        if (!$resultado['name']) {
            $error['name'] = 'Name must be 2 to 20 letters';
            $valido = false;
        }



        if (!$resultado['email']) {
            $error['email'] = 'error format email (example@example.com)';
            $valido = false;
        }




        if (!$resultado['address']) {
            $error['address'] = "Direction don't have points or symbols.";
            $valido = false;
        }

        if (!$resultado['last_name']) {
            $error['last_name'] = 'Last name must be 2 to 30 letters';
            $valido = false;
        }
        if (!$resultado['price']) {
            $error['price'] = 'Only numbers';
            $valido = false;
        }
      } else {
          $valido = false;
    };


    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}


//validate email
function valida_email($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}
